import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(!fighter)return "";
  const fighterImage = createFighterImage(fighter);
  const fighterName = createFighterName(fighter);
  const fighterStats = createFighterStats(fighter);
  

  fighterElement.append(fighterImage, fighterName, fighterStats);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterName(fighter) {
  const { name } = fighter;

  const nameElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___name'
  });

  nameElement.innerHTML = name;

  return nameElement;
}

export function createFighterStats(fighter) {
  const { health, attack, defense } = fighter;

  const statsElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats'
  });

  const healtElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___health'
  });
  const attackElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___attack'
  });
  const defenseElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___defense'
  });

  healtElement.innerHTML = `Health: ${health}`;
  attackElement.innerHTML = `Attack: ${attack}`;
  defenseElement.innerHTML = `Defence: ${defense}`;
  statsElement.append(healtElement,attackElement,defenseElement);

  return statsElement;
}

