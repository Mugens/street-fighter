import { createElement } from '../../helpers/domHelper';
import { showModal } from "./modal";
import App from '../../app';

export function showWinnerModal(fighter) {
  const body = createElement({tagName: 'div', className: 'modal-body'});
  body.innerText = `Winner is ${fighter.name}!`;
  showModal({title: `Winner is ${fighter.name}`,bodyElement: body})
}
