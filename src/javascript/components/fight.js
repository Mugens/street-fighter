import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  const playerOne = { 
    ...firstFighter,
    healthFull: firstFighter.health,
    combo: controls['PlayerOneCriticalHitCombination'].slice(),
    comboActive: true,
    healthBar: document.querySelector('#left-fighter-indicator'),
    block: false,
  };

  const playerTwo = { 
    ...secondFighter,
    healthFull: secondFighter.health,
    combo: controls['PlayerTwoCriticalHitCombination'].slice(),
    comboActive: true,
    healthBar: document.querySelector('#right-fighter-indicator'),
    block: false
  };

  let combo = [];

  return new Promise((resolve) => {

    window.addEventListener('keydown', (e)=>{
      const keyCode = e.code;
      let move = Object.keys(controls).find(key => controls[key]===keyCode);

      combo.push(keyCode);
      combo.splice(-playerOne.combo.length-1, combo.length-3);

      if(combo.join('').includes(playerOne.combo.join('')) && playerOne.comboActive){
        comboAttack(playerOne, playerTwo)
      }
      else if(combo.join('').includes(playerTwo.combo.join(''))  && playerTwo.comboActive){
        comboAttack(playerTwo, playerOne);
      }

      switch(move) {
        case "PlayerOneBlock":
          playerOne.block = true;
          break;
        case "PlayerTwoBlock":
          playerTwo.block = true;
          break;
      }
    });

    window.addEventListener('keyup', (e)=>{
      const keyCode = e.code;
      let move = Object.keys(controls).find(key => controls[key]===keyCode);


      if(!move) return;

      switch(move) {
        case 'PlayerOneAttack':
          attack(playerOne, playerTwo);
          break
        case 'PlayerTwoAttack':
          attack(playerTwo, playerOne);
            break
        case 'PlayerOneBlock':
          playerOne.block = false;
          break
        case 'PlayerTwoBlock':
          playerTwo.block = false;
          break
      }


      if(playerOne.health<=0)
        resolve(secondFighter);
      else if(playerTwo.health<=0)
        resolve(firstFighter);
      
    }); 
  });
}

export function attack(attacker, defender){
  if (defender.block || attacker.block) return;
  defender.health  -= getDamage(attacker, defender);
  console.log(defender.health);
  defender.healthBar.style.width = getPercent(defender);
}

export function comboAttack(attacker, defender) {
  defender.health  -= getSpecialDamage(attacker);
  defender.healthBar.style.width= getPercent(defender);
  
  attacker.comboActive= false;
  setTimeout(()=>attacker.comboActive=true, 10000);
}

export function getSpecialDamage(attacker) {
  return 2* attacker.attack;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender); 
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() +1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() +1;
  return fighter.defense * dodgeChance;
}

export function getPercent(target){
  let percent = 0;
  percent = ( target.health  / target.healthFull ) * 100;
  return `${percent<0 ? 0 : percent}%`;
}

